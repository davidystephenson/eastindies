var numplayers = 2
const getContract = message => `contract/${message}`

// Set dimensions
const dimensions = {}

const getDimension = (width, height) => ({
  width,
  height,
  halfWidth: width / 2,
  halfHeight: height / 2
})

const addDimensions = (bits, dimension) => bits.map(
  bit => { dimensions[bit] = dimension }
)

// Maps
const counts = [2, 3, 4]
counts.map(count => {
  const width = 1200 + count * 400

  dimensions[`map/${count}`] = getDimension(width, width / 2)
  dimensions[`map`] = getDimension(2200, 1600)
})

const contracts = ['industry', 'army', 'fleet', 'commander', 'merchant'].map(
  contract => `contract/${contract}`
)
addDimensions(
  ['good/1', 'gold/1', 'contract', ...contracts],
  getDimension(100, 100)
)

// Medium 
addDimensions(['good/5', 'gold/5', 'supporter', 'unit'], getDimension(150, 150))

// Utility boards
dimensions['market'] = getDimension(800, 1050)
dimensions['bank'] = getDimension(450, 1000)
dimensions['assetHolder'] = getDimension(400, 700)

// Access dimensions
const width = name => dimensions[name].width
const height = name => dimensions[name].height
const halfWidth = name => dimensions[name].halfWidth
const halfHeight = name => dimensions[name].halfHeight

// Place element
const center = (name, x, y, type = 'bit', count = 1) =>
  describeComponent(name, x, y, type, count)
const centerLeft = (name, x, y, type = 'bit', count = 1) =>
  describeComponent(name, x + halfWidth(name), y, type, count)
const centerRight = (name, x, y, type = 'bit', count = 1) =>
  describeComponent(name, x - halfWidth(name), y, type, count)
const topCenter = (name, x, y, type = 'bit', count = 1) =>
  describeComponent(name, x, y + halfHeight(name), type, count)
const bottomCenter = (name, x, y, type = 'bit', count = 1) =>
  describeComponent(name, x, y - halfHeight(name), type, count)

const topLeft = (name, x, y, type = 'bit', count = 1) =>
  describeComponent(name, x + halfWidth(name), y + halfHeight(name), type, count)
const topRight = (name, x, y, type = 'bit', count = 1) =>
  describeComponent(name, x - halfWidth(name), y + halfHeight(name), type, count)
const bottomLeft = (name, x, y, type = 'bit', count = 1) =>
  describeComponent(name, x + halfWidth(name), y - halfHeight(name), type, count)
const bottomRight = (name, x, y, type = 'bit', count = 1) =>
  describeComponent(name, x - halfWidth(name), y - halfHeight(name), type, count)

const column = (elements, x, y, orientation, buffer) =>
  elements.map(
    (element, index) =>
      describeComponent(element, x, y + buffer * index * orientation)
  )


const row = (elements, x, y, orientation, buffer) => 
  elements.map(
    (element, index) =>
      describeComponent(element, x + buffer * index * orientation, y)
  )

shuffle = v => {
    for(
      var j, x, i = v.length; i; j = parseInt(Math.random() * i),
      x = v[--i],
      v[i] = v[j],
      v[j] = x
    );

    return v;
};

const margin = 100
const innerMargin = 50
const innerBuffer = innerMargin / 2
const padded = size => size + innerMargin * 2

const setup = message => {
  console.log('message test:', message)
  const marketorder = message.marketorder.map(getContract)
  let players = message.numplayers
  numplayers = message.numplayers
  const mapName = 'map'

  console.log('players test:', players)

  const colors = message.colors.slice(0, players)

  console.log('colors test:', colors)

  const playerCounts = [...Array(players).keys()]
  const supporterCounts = playerCounts.map(count => count + 1)

  dimensions['centerpiece'] = getDimension(
    width(mapName) + margin + width('market') + margin + width('bank'),
    margin + height(mapName) + margin
  )
  const centerpieceBottom = halfHeight('centerpiece') + innerMargin 

  const mapCenter = -margin - halfWidth('market') - halfWidth('bank')

  const map = () => describeComponent(mapName, mapCenter, 0, 'board')
  const mapRight = mapCenter + halfWidth(mapName)
  
  const bankLeft = mapRight
  const bank = () => centerLeft('bank', bankLeft, 0, 'board')

  const marketLeft = mapRight + margin + width('bank') + margin
  const marketTop = -halfHeight('market')
  const market = () => describeComponent(
    'market',
    marketLeft + halfWidth('market'),
    0,
    'board'
  )

  const lot = (columnLeft, contracts) => contracts.map(
    (contract, index) =>
      describeComponent(
        contract,
        columnLeft + innerMargin + halfWidth('contract'),
        marketTop + (height('contract') + innerBuffer) * index + innerMargin + halfHeight('contract'),
        "board"
      )
  )
  const lots = []
  console.log('marketorder test:', marketorder)
  playerCounts.map(count =>
    lot(
      marketLeft + ((innerMargin * 2 + width('contract')) * count),
      marketorder.slice(count * 5, (count + 1) * 5 )
    ).map(lot => lots.push(lot))
  )

  const supporters = []
  const cardCounts = [...supporterCounts].reverse()
  cardCounts.map((count, index) => {
    const x = marketLeft + innerBuffer + index * (width('supporter') + innerMargin) + halfWidth('supporter')
    const y = -halfHeight('market') - innerMargin - halfHeight('supporter')

    supporters.push(
      describeComponent(`supporter/${count}/back`, x, y),
      describeComponent(`supporter/${count}/front`, x, y)
    )
  })

  const tableaus = { top: [], bottom: [] }
  playerCounts.map(
    count => count % 2 === 0 ?
      tableaus.top.push(colors[count]) : tableaus.bottom.push(colors[count])
  )

  const assetHolders = []
  const assets = [] 

  const assetHolder = (x, y, orientation) => 
    describeComponent(
      'assetHolder',
      x,
      orientation === 1 ? y : y + 1000,
      'board',
      1,
      270 + 90 * orientation
    )

  const unit = (color, x, y, front = true) =>
    describeComponent(`unit/${color}/${front ? 'front' : 'back'}`, x, y, 'bit', 25)

  const industry = (color, x, y) => asset('industry', color, x, y)

  const tableau = (color, start, end, inside, orientation) => {
    const getAssetMargin = index =>
      innerMargin + halfWidth('unit') + (halfWidth('assetHolder')) * index

    const getAssetX = index => end - getAssetMargin(index) * orientation

    const assetsY = inside + (height('unit') - 50.5) * orientation

    const describeGold = (size, x, y) => describeComponent(`gold/${size}`, x, y)
    const describeTreasury = (size, inside, count) =>
      [...Array(count).keys()].map(
        index =>
          describeGold(
            size,
            end - (halfWidth(`gold/${size}`) + innerMargin * (index + 1)) * orientation,
            inside
          )
      )

    const assetHolderY = inside + (halfHeight('assetHolder') * orientation)

    const midX = start + ((end - start) / 2)
    const midY = inside + halfHeight('assetHolder') * orientation

    assetHolders.push(
      assetHolder(
        end - halfWidth('assetHolder') * orientation,
        assetHolderY,
        orientation
      )
    )

    const startingComponents = [
      unit(color, getAssetX(0.5), assetsY),
      unit(color, getAssetX(0.5), assetsY, false),
      ...describeTreasury(
        1,
        inside + (innerMargin + halfWidth('gold/1') + 225) * orientation,
        5
      ),
      ...describeTreasury(5, inside + 575 * orientation, 4),
      describeComponent('good/1', midX, midY),
      ...row(
        contracts,
        midX - 200 * orientation,
        midY - 200 * orientation,
        orientation,
        width('contract')
      ),
      ...column(
        Array(4).fill('contract/industry'),
        midX - 200 * orientation,
        midY - 100 * orientation,
        orientation,
        width('contract')
      ),
    ]
      
    startingComponents.map(bit => assets.push(bit))
  }

  const describeTableaus = (colors, orientation) => {
    const tableauWidth = (width('centerpiece') / colors.length)
    const orientedWidth = tableauWidth * orientation
    const baseX = -halfWidth('centerpiece') * orientation
    const inside = centerpieceBottom * orientation

    return colors.map(
      (color, index) => tableau(
        color,
        baseX + index * orientedWidth,
        baseX + (index + 1) * orientedWidth,
        inside,
        orientation
      )
    )
  }

  describeTableaus(tableaus.bottom, 1)
  describeTableaus(tableaus.top, -1)

  const supply = () => describeComponent(
    'good/1',
    marketLeft + innerMargin + halfWidth('good/1'),
    marketTop + halfHeight('market') + 250 + innerMargin * 2 + halfWidth('good/1')
  )

  const stack = (bit, x, y, count) => describeComponent(bit, x, y, 'bit', count)

  const bankMargin = 85.2
  const bankBuffer = bankMargin / 2
  const columnTop = -halfHeight('bank') + innerMargin + halfHeight('contract')
  const describeColumn = (x, stacks) => stacks.map(
    (stackData, index) =>
      stack(
        stackData.bit,
        x,
        -halfHeight('bank') + (index + 0.5) * (height('bank') / stacks.length),
        stackData.count
      )
  )

  const leftColumn = bankLeft + innerMargin + halfWidth('contract')
  const rightColumn = leftColumn + innerMargin * 3 + halfWidth('gold/5')
  const columns = [
    ...describeColumn(
      leftColumn,
      [
        { bit: 'contract/industry', count: 100 },
        { bit: 'contract/merchant', count: 100 },
        { bit: 'contract/fleet', count: 25 },
        { bit: 'contract/army', count: 25 },
        { bit: 'contract/commander', count: 25 }
      ]
    ),
    ...describeColumn(
      rightColumn,
      [
        { bit: 'good/1', count: 25 - players + 1 },
        { bit: 'good/5', count: 25 },
        { bit: 'gold/1', count: 50 - players * 5 },
        { bit: 'gold/5', count: 50 - players * 4 }
      ]
    )
  ]

  componentList = [
    map(),
    bank(),
    market(),
    ...lots,
    ...assetHolders,
    ...supporters,
    ...assets,
    ...columns,
    supply(),
  ]

  console.log('list test:', componentList)
  loadComponents(componentList,message.updates);
}
